package Control;

import java.util.HashSet;
import java.util.Set;

import model.Pessoa;

public class Control {
	
	public static Control control = null;
	
	private Control(){}
	
	private Set<Pessoa> pessoas = new HashSet<Pessoa>();
	
	public static Control getInstance() throws Exception{
		if (control == null){
			control = new Control();
		}else{
			throw new Exception ("Controlador ja existe");
		}
		return control;
	}

}
