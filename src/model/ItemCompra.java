package model;

public class ItemCompra {
	
	private int quantidade;
	private float valor;
	
	private Produto produto = new Produto();
	
	public int getQuantidade(){
		return quantidade;
		
	}
	
	public void setQuantidade(int quantidade){
		this.quantidade = quantidade;
	}
	
	public float getValor(){
		return valor;
		
	}
	
	public void setValor(float valor){
		this.valor = valor;
	}
	
	public Produto getProduto(){
		return produto;
	}
	

}
