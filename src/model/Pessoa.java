package model;

public class Pessoa {
	
    private Compra num = new Compra();
	private String nome,sobrenome;
	
	public String getNome(){
		return nome;
	}
	public void setNome(String nome){
		this.nome = nome;
		
	}
	public String getSobrenome(){
		return sobrenome;
	}
	
	public void setSobrenome(String sobrenome){
		this.sobrenome = sobrenome;
	}
	public Compra getCompra(){
		return num;
	}
}
